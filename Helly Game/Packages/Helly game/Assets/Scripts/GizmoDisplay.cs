﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GizmoDisplay : MonoBehaviour
{
    public Color gizmoColor = Color.cyan;
    public Shapes gizmoShapes;
    public bool isShapeSolid = true;
    [Header("Sphere Set-up")]
    [Range(0.5f, 3f)]
    public float gizmoSize = 1f;
    [Header("Cube Set-up")]
    public BoxCollider2D highlightBoxCollider;
    public Vector3 cubeSize = new Vector3(1,1,1);
    [Header("Line Set-up")]
    [Tooltip("Will automatically set point one if this is set to an object`")]
    public GameObject pointOneReference;
    [Tooltip("Will automatically set point two if this is set to an object`")]
    public GameObject pointTwoReference;
    [Tooltip("Set a permanent location for the first point IF point one reference is not set")]
    public Vector3 pointOne;
    [Tooltip("Set a permanent location for the second point IF point two reference is not set")]
    public Vector3 pointTwo;


    Vector3 position = Vector3.zero; //delete this


    private void Awake()
    {
        pointOne = transform.position;
        pointTwo = transform.position + Vector3.up;

    }
    private void OnDrawGizmos()
    {
        Gizmos.color = gizmoColor;
        switch (gizmoShapes)
        {
            case Shapes.sphere:
                if (isShapeSolid)
                {
                    Gizmos.DrawSphere(transform.position, gizmoSize);
                }
                else
                {
                    Gizmos.DrawWireSphere(transform.position, gizmoSize);
                }
                break;
            case Shapes.cube:
                if (isShapeSolid || highlightBoxCollider != null)
                    {
                    if(highlightBoxCollider != null)
                    {
                        cubeSize = new Vector3(highlightBoxCollider.size.x, highlightBoxCollider.size.y, 1);
                    }
                        Gizmos.DrawCube(transform.position, cubeSize);
                    }
                    else
                    {
                        Gizmos.DrawWireCube(transform.position, cubeSize);
                    }
                
                break;
            case Shapes.line:
                if (pointOneReference == null)
                {
                    pointOne = transform.position;
                }
                else
                {
                    pointOne = pointOneReference.transform.position;
                }
                if(pointTwoReference != null)
                {
                    pointTwo = pointTwoReference.transform.position;
                }
                Gizmos.DrawLine(pointOne, pointTwo);
                break;
        }


    }
    private void Update()
    {

        if(transform.position != position)
        {

        }
    }

}

public enum Shapes {
    sphere,
    cube,
    line
}
