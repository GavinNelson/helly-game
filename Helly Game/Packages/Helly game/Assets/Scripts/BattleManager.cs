﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


public class BattleManager : MonoBehaviour

{

    #region Variable Declarations

    [Tooltip("When the battle starts, the camera will increase it's size by this multiple")]
    public float battleFieldCameraScale = 1f;
    public float battleStartDelay = 1f;
    public float timeBetweenWaves = 1f;
    [Range(0f, 1f)]
    [Tooltip("If this value is 0, the camera scale speed will not change from default. If it is not 0, " +
        "it will change the speed that the camera changes scale when battle begins." +
        " Default value is found on the 'Camera Logic' script on the main camera")]
    public float substituteCameraScaleSpeed;
    int k = 0;

    bool battleActive = false;
    bool waveSpawning;
    bool battleStarted = false;
    bool battleCompleted = false;
    bool timerComplete = false;

    int selectedSpawnLocation;

    List<GameObject> activeEnemies;

    #region Waves array declaration


    GameObject[] nextWave;
    int currentWave = 1;

    WaveSetup waves;

    List<int> waveSpawnLocations;

    [Header("All objects must be set")]
    public Camera mainCamera;
    [Tooltip("This will be the parent of the spawned enemies")]
    public GameObject enemyList;
    [Tooltip("This is the trigger to start the battle. By default it is a child of this object")]
    public GameObject battleTrigger;

    #endregion
    #endregion




    private void Awake()
    {
        waves = this.GetComponent<WaveSetup>();
        activeEnemies = new List<GameObject>();

    }

    void Start()
    {
        waveSpawning = false;
        nextWave = waves.wave1;
        waveSpawnLocations = waves.wave1SpawnLocs;
        SendMessageUpwards("AddBattleInstanceToList", this);
    }

    void Update()
    {
        if (activeEnemies.Count > 0) //clear dead enemies out of the active enemy list
        {
            if (activeEnemies[0] == null)
            {
                activeEnemies.Remove(activeEnemies[0]);
            }
        }
    }

    GameObject[] SetNextWave() //Set all variables for the next wave of enemies.
    {
        GameObject[] setWave = null;
        if (currentWave < waves.numberOfWaves)
        {
            switch (currentWave)
            {
                case 1:
                    setWave = waves.wave2;
                    waveSpawnLocations = waves.wave2SpawnLocs;
                    Debug.Log("wave 1 complete");
                    break;
                case 2:
                    setWave = waves.wave3;
                    waveSpawnLocations = waves.wave3SpawnLocs;
                    Debug.Log("wave 2 complete");
                    break;
                case 3:
                    setWave = waves.wave4;
                    waveSpawnLocations = waves.wave4SpawnLocs;
                    Debug.Log("wave 3 complete");
                    break;
                case 4:
                    setWave = waves.wave5;
                    waveSpawnLocations = waves.wave5SpawnLocs;
                    Debug.Log("wave 4 complete");
                    break;
                case 5:
                    setWave = waves.wave6;
                    waveSpawnLocations = waves.wave6SpawnLocs;
                    Debug.Log("wave 5 complete");
                    break;
                case 6:
                    setWave = waves.wave7;
                    waveSpawnLocations = waves.wave7SpawnLocs;
                    Debug.Log("wave 6 complete");
                    break;
                case 7:
                    setWave = waves.wave8;
                    waveSpawnLocations = waves.wave8SpawnLocs;
                    Debug.Log("wave 7 complete");
                    break;
                case 8:
                    setWave = waves.wave9;
                    waveSpawnLocations = waves.wave9SpawnLocs;
                    Debug.Log("wave 8 complete");
                    break;
                case 9:
                    setWave = waves.wave10;
                    waveSpawnLocations = waves.wave10SpawnLocs;
                    Debug.Log("wave 9 complete");
                    break;

            }
            currentWave++;
        }
        else
        {
            return null;
        }
        return setWave;
    }

    IEnumerator NextWaveTimer(float timeTillNextWave)//delay between waves
    {
        GameObject[] nextWave = SetNextWave();
        yield return new WaitForSecondsRealtime(1f);
        StartCoroutine(SpawnWave(nextWave));

    }
    IEnumerator SpawnWave(GameObject[] currentActiveWave)
    {
        if (waveSpawning == false) //reset the wave unit counter if it is a new wave
        {
            k = 0;
            waveSpawning = true;
            StartCoroutine(Timer(waves.timer[currentWave - 1]));
        }
        if (k < waveSpawnLocations.Count)
        {
            if (waveSpawnLocations[k] == 0)
            {
                waveSpawnLocations[k] = Random.Range(1, waves.spawnLocations.Length +1);
            }
        }
        yield return new WaitForSecondsRealtime(waves.spawnInterval[currentWave-1]);
        if (battleActive && waveSpawning)
        {
            if (currentActiveWave == null)
            {
               StartCoroutine(EndBattle());
            }
            else if (k == currentActiveWave.Length)
            {
                StartCoroutine(CheckNextWaveConditions());
                waveSpawning = false;
            }
            else
            {
                selectedSpawnLocation = waveSpawnLocations[k];
                GameObject nextEnemy = Instantiate(currentActiveWave[k], waves.spawnLocations[selectedSpawnLocation -1].transform.position, transform.rotation, enemyList.transform);
                activeEnemies.Add(nextEnemy);
                k++;
                StartCoroutine(SpawnWave(currentActiveWave));
            }
        }

    }


    void StartBattle()//initial battle setup
    {
        if (battleCompleted == false && battleStarted == false)
        {
            currentWave = 1;
            if(substituteCameraScaleSpeed != 0)
            {
                mainCamera.SendMessage("SetCameraScaleSpeed", substituteCameraScaleSpeed);
            }
            mainCamera.SendMessage("SetNewCameraTarget", this.gameObject);
            mainCamera.SendMessage("ScaleCameraTo", battleFieldCameraScale);
            mainCamera.SendMessage("ActivateBorders");
            SendMessageUpwards("DeactivateInactiveBattles");
            StartCoroutine(SpawningBegins());
            Destroy(battleTrigger);
        }
    }
    IEnumerator SpawningBegins()
    {
        yield return new WaitForSecondsRealtime(battleStartDelay);

        if (battleCompleted == false)
        {
            battleStarted = false;
            battleActive = true;
            waveSpawning = true;
            StartCoroutine(SpawnWave(waves.wave1));
            StartCoroutine(Timer(waves.timer[0]));
        }
        else
        {
            Debug.Log("Battle has already been completed - make sure Battle Trigger is set in inspector");
        }
    }
IEnumerator EndBattle()//reset variables to regular play variables
    {
        yield return new WaitForSecondsRealtime(0.5f);
        if (activeEnemies.Count == 0)
        {
            mainCamera.SendMessage("ResetCameraScaleSpeed");
            Debug.Log("This battle is over");
            battleCompleted = true;
            battleActive = false;
            mainCamera.SendMessage("ResetCameraTarget");
            mainCamera.SendMessage("ResetCameraScale");
            mainCamera.SendMessage("DeactivateBorders");
            SendMessageUpwards("ReactivateBattles");
        } else
        {

            StartCoroutine(EndBattle());
        }
    }

    IEnumerator CheckNextWaveConditions()//check to see all conditions are met to start the next wave
    {
        yield return new WaitForSecondsRealtime(0.1f);
        if (timerComplete && activeEnemies.Count <= waves.enemiesRemaining[currentWave - 1])
        {
            StartCoroutine(NextWaveTimer(timeBetweenWaves));
        }else
        {
            StartCoroutine(CheckNextWaveConditions());
        }
    }
    IEnumerator Timer(float time)// timer for coditional wave spawning
    {
        timerComplete = false;
        yield return new WaitForSecondsRealtime(time);
        timerComplete = true;
    }

    void DeactivateBattleTrigger()
    {
        if (battleTrigger != null)
        {
            battleTrigger.SetActive(false);
        }
    }

    void ReactivateBattleTrigger()
    {
        if (battleTrigger != null) 
        {
            battleTrigger.SetActive(true);
        }
    }
}
