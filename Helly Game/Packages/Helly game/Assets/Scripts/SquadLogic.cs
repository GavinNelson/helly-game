﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquadLogic : MonoBehaviour
{


    [Header("Squad Finder Variables")]
    [Tooltip("This is the max range that this enemy will search for a squad around itself")]
    public float maxLFGRadius = 3;
    [Tooltip("This will be the size that the search radius starts at/returns to. Increase for larger enemies")]
    public float minLFGRadius = 0.1f;


    [Header("Squad Variables")]
    [Tooltip("This is the space that this unit offers in its squad. The enemy with the largest value will become the leader, increasing the squads max size")]
    public int maxSquadSizeIfLeader = 5;
    [Tooltip("This is how much space that this unit requires in order to join a squad")]
    public int SizeInSquad = 1;
    public bool isLeader = false;
    public bool isSquaddie = false;


    [Header("debugging ---- Do not touch")]
    public GameObject leader;
    public List<GameObject> squad;
    public int spaceLeftInSquad;
    public bool lookingForGroup = true;
    public bool confirmedInSquad = true;

    public int referenceNumber;

    CircleCollider2D squadTigger;
    GizmoDisplay connectionToLeader;
    SquadLogic leaderLogic;

    private void Awake()
    {
        squadTigger = GetComponent<CircleCollider2D>();
        connectionToLeader = GetComponent<GizmoDisplay>();
        referenceNumber = Random.Range(1, 100);
    }
    // Start is called before the first frame update
    void Start()
    {
        squadTigger.radius = minLFGRadius;
        spaceLeftInSquad = maxSquadSizeIfLeader;
        StartCoroutine("SquadFinderRadiusExpand");
        StartCoroutine("CheckIfStillInSquad");
    }

    // Update is called once per frame
    void Update()
    {
        if (isSquaddie && leader == null)
        {
            isSquaddie = false;
        }
        if (!isSquaddie)
        {
            isLeader = true;
        }
        else
        {
            isLeader = false;
        }

        if ((isSquaddie || (isLeader && spaceLeftInSquad == 0)) && lookingForGroup)
        {
            lookingForGroup = false;
            StopCoroutine("SquadFinderRadiusExpand");
            squadTigger.radius = minLFGRadius;
        } else if(!lookingForGroup && isLeader)
        {
            lookingForGroup = true;
            squadTigger.radius = minLFGRadius;
            StartCoroutine("SquadFinderRadiusExpand");
        }

        if (isLeader)
        {
            connectionToLeader.pointTwoReference = gameObject;
            RollCall();
            CheckSquadSize();
            CheckForNewLeadership();
            confirmedInSquad = false;
        }else if (isSquaddie)
        {
            connectionToLeader.pointTwoReference = leader;
            if (confirmedInSquad == false && leader != null)
            {
                leader.SendMessage("ForceNewSquaddie", gameObject);
                confirmedInSquad = true;
            }
        }

    }


    //give radar effect to the looking for group trigger
    IEnumerator SquadFinderRadiusExpand()
    {
        yield return new WaitForSeconds(0.01f);
        squadTigger.radius += 0.03f;
        if (squadTigger.radius > maxLFGRadius)
        {
            squadTigger.radius = minLFGRadius;
        }
        StartCoroutine("SquadFinderRadiusExpand");
    }

    void ForceNewSquaddie(GameObject newSquaddie)
    {
        squad.Add(newSquaddie);
    }
    //This function will make sure that it is the leader of all of it's squaddies and make sure that there are no duplicates in the squad.
    void RollCall()
    {

        for (int i = 0; i < squad.Count; i++)
        {
            SquadLogic tempSquadLogic = squad[i].GetComponent<SquadLogic>();
            for (int k = 0; k < squad.Count; k++)
            {
                if(k != i && squad[i] == squad[k])
                {
                    Debug.Log("duplicate found in squad");
                    squad.RemoveAt(k);
                }
            }
            if (!tempSquadLogic.ConfirmLeader(this.gameObject))
            {
                squad.RemoveAt(i);
                i--;
            }
        }
    }

    //This will check how much space is left in the squad
    void CheckSquadSize()
    {
        int runningCount = 0;

        for (int i = 0; i < squad.Count; i++)
        {
            SquadLogic tempSquadLogic = squad[i].GetComponent<SquadLogic>();
            runningCount += tempSquadLogic.SizeInSquad;
        }
        spaceLeftInSquad = maxSquadSizeIfLeader - runningCount;
    }

    void BecomeSquaddie(GameObject newLeader)
    {
        leader = newLeader;
        isSquaddie = true;
    }

    void AddSquaddie(GameObject newRecruit)
    {
        SquadLogic tempSquadLogic = newRecruit.GetComponent<SquadLogic>();
        if (!lookingForGroup || !tempSquadLogic.lookingForGroup)//Both sides must be looking for a group. 
        {
            return;
        }
        while(tempSquadLogic.referenceNumber == referenceNumber)//make sure both scripts have a different reference number
        {
            referenceNumber = Random.Range(1, 100);
        }

        if(tempSquadLogic.maxSquadSizeIfLeader > maxSquadSizeIfLeader)// If the recruit can hold a larger group, exit the function. The recruit will continue their function instead
        {
            return;
        }
        //If both targets can hold same army, the bigger reference number will continue the function. We have already confirmed that they will never be equal
        if (tempSquadLogic.referenceNumber > referenceNumber && tempSquadLogic.maxSquadSizeIfLeader == maxSquadSizeIfLeader)
        {
            return;
        }
        if ((tempSquadLogic.maxSquadSizeIfLeader - tempSquadLogic.spaceLeftInSquad + tempSquadLogic.SizeInSquad) > spaceLeftInSquad)//Check if there is space for the merger
        {
            return;
        }

        while (tempSquadLogic.squad.Count > 0)
        {
            squad.Add(tempSquadLogic.squad[0]);
            tempSquadLogic.squad[0].SendMessage("BecomeSquaddie", gameObject);
            tempSquadLogic.squad.RemoveAt(0);
        }
        tempSquadLogic.BecomeSquaddie(gameObject);
        squad.Add(tempSquadLogic.gameObject);
        

    }

    //This checks to see if there is a more worthy leader in the group
    void CheckForNewLeadership()
    {
       if(squad.Count > 0)
        {
            for( int i = 0; i < squad.Count; i++)
            {
                SquadLogic tempSquadLogic = squad[i].GetComponent<SquadLogic>();
                if(tempSquadLogic.maxSquadSizeIfLeader > maxSquadSizeIfLeader)
                {
                    confirmedInSquad = false;
                    tempSquadLogic.isSquaddie = false;
                    while (squad.Count != 0)
                    {
                        if(tempSquadLogic == squad[0])
                        {
                            squad.RemoveAt(0);
                        }else
                        {
                            tempSquadLogic.squad.Add(squad[0]);
                            squad[0].SendMessage("BecomeSquaddie", tempSquadLogic.gameObject);
                            squad.RemoveAt(0);
                        }
                    }
                    tempSquadLogic.isSquaddie = false;
                    isSquaddie = true;
                    leader = tempSquadLogic.gameObject;
                    leader.SendMessage("AddSquaddie", gameObject);
                    return;
                }
            }
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if( collision.tag == "Enemy")
        {
            AddSquaddie(collision.gameObject);
        }
    }



    //a leader will call this function of a squaddie. If the squaddie  recognizes that leader as their leader, it will return true and nothing happens.
    //If it returns false, the leader will remove that squaddie from their list of squaddies.
    bool ConfirmLeader(GameObject leaderCheck)
    {
        if (isLeader)
        {
            return false;
        }

        if (leaderCheck == leader)
        {
            return true;
        }else
        {
            return false;
        }
    }
    bool ConfirmIsLeadersSquaddie(GameObject squaddie)
    {
        bool isInSquad = false;
         for (int i = 0; i < squad.Count; i++)
        {
            if(squad[i] == squaddie)
            {
                isInSquad = true;
            }
        }
        return isInSquad;
    }
    IEnumerator CheckIfStillInSquad()
    {
        if(leader != null)
        {
            leaderLogic = GetComponent<SquadLogic>();
            if (!leaderLogic.ConfirmIsLeadersSquaddie(gameObject))
            {
                if (leaderLogic.spaceLeftInSquad >= SizeInSquad)
                {
                    leaderLogic.SendMessage("ForceNewSquaddie", gameObject);
                }
                else
                {
                    leader = null;
                    isSquaddie = false;
                }
            }
        }
        yield return new WaitForSeconds(2);
        StartCoroutine("CheckIsStillInSquad");
    }
}
