﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelBattleInstanceManager : MonoBehaviour
{
    /// <summary>
    ///There wil only be one of these in each level. It gives you control of all the battles in  one location. This will be used to deactivate 
    ///battles when one is active.
    /// </summary>
    public List<BattleManager> allBattlesInLevel;

    void AddBattleInstanceToList(BattleManager newItem)
    {
        allBattlesInLevel.Add(newItem);
    }

    void DeactivateInactiveBattles()
    {
        for (int i = 0; i < allBattlesInLevel.Count; i++)
        {
            allBattlesInLevel[i].SendMessage("DeactivateBattleTrigger");
        }
    }

    void ReactivateBattles()
    {
        for(int i = 0; i < allBattlesInLevel.Count; i++)
        {
            allBattlesInLevel[i].SendMessage("ReactivateBattleTrigger");
        }
    }

}
