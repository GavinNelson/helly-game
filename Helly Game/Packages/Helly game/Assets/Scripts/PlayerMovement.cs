﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    
    public float maxSpeed;
    public float accelRate;

    float horizontal;
    float vertical;
    

    Vector3 movement;

    //bool moving;

    Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    
    void Update()
    {
        vertical = Input.GetAxisRaw("Vertical");
        horizontal = Input.GetAxisRaw("Horizontal");

        movement = new Vector3(horizontal, vertical, 0);

        movement.Normalize();

        movement *= accelRate;

        /*might need this later
        if (horizontal != 0 || vertical != 0)
        {
            moving = true;
        }
        else
        {
            moving = false;
        }*/
    }

    private void FixedUpdate()
    {
        if (Mathf.Abs(rb.velocity.x) < maxSpeed) { horizontal = 0; }
        if (Mathf.Abs(rb.velocity.x) < maxSpeed) { vertical = 0; }
        
            rb.velocity = new Vector2(rb.velocity.x + movement.x, rb.velocity.y + movement.y);
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "BattleSequence")
        {
            collision.SendMessageUpwards("StartBattle");
        }
    }



}