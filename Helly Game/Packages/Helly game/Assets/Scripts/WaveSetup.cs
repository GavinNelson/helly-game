﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[ExecuteInEditMode]
public class WaveSetup : MonoBehaviour
{


    #region Variable Declaration
    [Header("Wave setup")]
    [Range(1, 10)]
    public int numberOfWaves = 1; 
    public GameObject[] spawnLocations; //Spawn locations for enemies

    [Header("Wave Start Conditions")]
    [Tooltip("How many enemies remaining before the next wave can spawn")]
    public int[] enemiesRemaining;
    [Tooltip("What is the MINIMUM amount of time that must pass before the next wave spawns")]
    public float[] timer;
    [Tooltip("How Long will it take for each enemy to spawn in a given wave")]
    public float[] spawnInterval;
    [Header("Wave Composition setup")]

    public GameObject[] wave1;
    [Tooltip("If spawn location is 0, It will be randomly chosen on spawn")]
    public List<int> wave1SpawnLocs;

    public GameObject[] wave2;
    [Tooltip("If spawn location is 0, It will be randomly chosen on spawn")]
    public List<int> wave2SpawnLocs;

    public GameObject[] wave3;
    [Tooltip("If spawn location is 0, It will be randomly chosen on spawn")]
    public List<int> wave3SpawnLocs;

    public GameObject[] wave4;
    [Tooltip("If spawn location is 0, It will be randomly chosen on spawn")]
    public List<int> wave4SpawnLocs;

    public GameObject[] wave5;
    [Tooltip("If spawn location is 0, It will be randomly chosen on spawn")]
    public List<int> wave5SpawnLocs;

    public GameObject[] wave6;
    [Tooltip("If spawn location is 0, It will be randomly chosen on spawn")]
    public List<int> wave6SpawnLocs;

    public GameObject[] wave7;
    [Tooltip("If spawn location is 0, It will be randomly chosen on spawn")]
    public List<int> wave7SpawnLocs;

    public GameObject[] wave8;
    [Tooltip("If spawn location is 0, It will be randomly chosen on spawn")]
    public List<int> wave8SpawnLocs;

    public GameObject[] wave9;
    [Tooltip("If spawn location is 0, It will be randomly chosen on spawn")]
    public List<int> wave9SpawnLocs;

    public GameObject[] wave10;
    [Tooltip("If spawn location is 0, It will be randomly chosen on spawn")]
    public List<int> wave10SpawnLocs;

    List<List<int>> spawnLocLists;
    bool gameStarted = false;
    #endregion



    private void Start()
    {
        gameStarted = true;
    }

    void Update()
    {
        //if (gameStarted)
        {
            #region Keep spawn list consistent length of enemy list

            if (wave1.Length > wave1SpawnLocs.Count)
            {

                wave1SpawnLocs.Add(0);

            }
            if (wave1.Length < wave1SpawnLocs.Count)
            {

                wave1SpawnLocs.RemoveAt(wave1SpawnLocs.Count - 1);

            }
            if (wave2.Length > wave2SpawnLocs.Count)
            {

                wave2SpawnLocs.Add(0);

            }
            if (wave2.Length < wave2SpawnLocs.Count)
            {

                wave2SpawnLocs.RemoveAt(wave2SpawnLocs.Count - 1);

            }
            if (wave3.Length > wave3SpawnLocs.Count)
            {

                wave3SpawnLocs.Add(0);

            }
            if (wave3.Length < wave3SpawnLocs.Count)
            {

                wave3SpawnLocs.RemoveAt(wave3SpawnLocs.Count - 1);

            }
            if (wave4.Length > wave4SpawnLocs.Count)
            {

                wave4SpawnLocs.Add(0);

            }
            if (wave4.Length < wave4SpawnLocs.Count)
            {

                wave4SpawnLocs.RemoveAt(wave4SpawnLocs.Count - 1);

            }
            if (wave5.Length > wave5SpawnLocs.Count)
            {

                wave5SpawnLocs.Add(0);

            }
            if (wave5.Length < wave5SpawnLocs.Count)
            {

                wave5SpawnLocs.RemoveAt(wave5SpawnLocs.Count - 1);

            }
            if (wave6.Length > wave6SpawnLocs.Count)
            {

                wave6SpawnLocs.Add(0);

            }
            if (wave6.Length < wave6SpawnLocs.Count)
            {

                wave6SpawnLocs.RemoveAt(wave6SpawnLocs.Count - 1);

            }
            if (wave7.Length > wave7SpawnLocs.Count)
            {

                wave7SpawnLocs.Add(0);

            }
            if (wave7.Length < wave7SpawnLocs.Count)
            {

                wave7SpawnLocs.RemoveAt(wave7SpawnLocs.Count - 1);

            }
            if (wave8.Length > wave8SpawnLocs.Count)
            {

                wave8SpawnLocs.Add(0);

            }
            if (wave8.Length < wave8SpawnLocs.Count)
            {

                wave8SpawnLocs.RemoveAt(wave8SpawnLocs.Count - 1);

            }
            if (wave9.Length > wave9SpawnLocs.Count)
            {

                wave9SpawnLocs.Add(0);

            }
            if (wave9.Length < wave9SpawnLocs.Count)
            {

                wave9SpawnLocs.RemoveAt(wave9SpawnLocs.Count - 1);

            }
            if (wave10.Length > wave10SpawnLocs.Count)
            {

                wave10SpawnLocs.Add(0);

            }
            if (wave10.Length < wave10SpawnLocs.Count)
            {

                wave10SpawnLocs.RemoveAt(wave10SpawnLocs.Count - 1);

            }
            //Refresh if the values are not equal. Editor will only run 1 frame at a time normally
            if (wave1.Length != wave1SpawnLocs.Count || wave2.Length != wave2SpawnLocs.Count || wave3.Length != wave3SpawnLocs.Count || wave4.Length != wave4SpawnLocs.Count || wave5.Length != wave5SpawnLocs.Count || wave6.Length != wave6SpawnLocs.Count || wave7.Length != wave7SpawnLocs.Count || wave8.Length != wave8SpawnLocs.Count || wave9.Length != wave9SpawnLocs.Count || wave10.Length != wave10SpawnLocs.Count)
            {
                Update();
            }
            #endregion

            #region Validate spawn locations
            if (spawnLocLists == null)
            {
                #region SpawnLocList array creation
                spawnLocLists = new List<List<int>>();
                spawnLocLists.Add(wave1SpawnLocs);
                spawnLocLists.Add(wave2SpawnLocs);
                spawnLocLists.Add(wave3SpawnLocs);
                spawnLocLists.Add(wave4SpawnLocs);
                spawnLocLists.Add(wave5SpawnLocs);
                spawnLocLists.Add(wave6SpawnLocs);
                spawnLocLists.Add(wave7SpawnLocs);
                spawnLocLists.Add(wave8SpawnLocs);
                spawnLocLists.Add(wave9SpawnLocs);
                spawnLocLists.Add(wave10SpawnLocs);
                #endregion
            }
            for (int i = 0; i < spawnLocLists.Count; i++)
            {
                for (int k = 0; k < spawnLocLists[i].Count; k++)
                {
                    if (spawnLocLists[i][k] > spawnLocations.Length)
                    {
                        spawnLocLists[i][k] = spawnLocations.Length;
                    }
                }
            }
            #endregion
        }

    }
}
