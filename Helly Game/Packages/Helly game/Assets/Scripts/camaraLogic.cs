﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camaraLogic : MonoBehaviour
{
    [Range(0.01f, 1f)]
    public float walkingScrollSpeed = 0.1f;
    [Range(0.01f, 1f)]
    public float newTargetScrollSpeed = 0.05f;
    [Range(0.01f, 1f)]
    public float cameraScaleSpeed = 0.1f;
    float defaultCameraScaleSpeed;
    float scrollSpeed;
    GameObject cameraTarget;

    public GameObject player;
    Vector3 distanceToTarget;

    float targetCameraScale = 1;
    float currentScale = 1;

    Camera cameraComponent;
    public GameObject[] borders;


    private void Awake()
    {
        cameraTarget = player;
        scrollSpeed = walkingScrollSpeed;
        cameraComponent = GetComponent<Camera>();
        defaultCameraScaleSpeed = cameraScaleSpeed;
    }

    private void Update()
    {
        distanceToTarget = cameraTarget.transform.position - transform.position;
        distanceToTarget.z = 0;
        transform.position = transform.position + distanceToTarget * scrollSpeed;
        currentScale = targetCameraScale - transform.localScale.x;
        transform.localScale += new Vector3(currentScale, currentScale, 0) * cameraScaleSpeed;
        cameraComponent.orthographicSize += 5 * currentScale * cameraScaleSpeed;
    }

    void SetNewCameraTarget(GameObject newTarget)
    {
        scrollSpeed = newTargetScrollSpeed;
        cameraTarget = newTarget;
    }

    void ResetCameraTarget()
    {
        scrollSpeed = walkingScrollSpeed;
        cameraTarget = player;
    }

    void ResetCameraScale()
    {
        targetCameraScale = 1f;
    }

    void ScaleCameraTo(float scale)
    {
        targetCameraScale = scale;
    }
    void SetCameraScaleSpeed(float newSpeed)
    {
        cameraScaleSpeed = newSpeed;
    }

    void ResetCameraScaleSpeed()
    {
        cameraScaleSpeed = defaultCameraScaleSpeed;
    }
    void ActivateBorders()
    {
        for(int i = 0; i < borders.Length; i++)
        {
            borders[i].SetActive(true);
        }
    }

    void DeactivateBorders()
    {
        for (int i = 0; i < borders.Length; i++)
        {
            borders[i].SetActive(false);
        }
    }
}
